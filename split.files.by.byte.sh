#!/bin/bash
#
# Written Dec, 2015 to solve NCL challenge

# Splits a binary file by alternating bytes.  Odd bytes go to odd.bin
# while even bytes are appended to even.bin
#
# USAGE: split.files.by.byte.sh [FILE] [FILE SIZE IN BYTES]

if [[ $1 == "-h" ]]; then
    echo "USAGE: ./split.files.by.byte.sh [FILE] [FILE SIZE IN BYTES]"
    exit
fi

FILE=$1
MAXC=$2
rm -f even.bin
rm -f odd.bin
counter=0
while [[ $counter -lt $MAXC ]]; do
    byte_num=$counter
    if [ $byte_num -eq 0 ]; then
        counter=$((counter+1))
        dd if=$FILE bs=1 count=1>>even.bin
    elif [ $((byte_num%2)) -eq 0 ]; then
        counter=$((counter+1))
        dd if=$FILE bs=1 skip=$byte_num count=1>>even.bin
    else
        dd if=$FILE bs=1 skip=$byte_num count=1>>odd.bin
        counter=$((counter+1))
    fi
done
