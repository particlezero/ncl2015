#!/bin/bash

# Script to return hex and binary equivalents for each octet of an IP address using bc.
# Purpose: refresh memory on various scripting concepts, including the forced printing of
# leading zeros on output and general use of awk.
#
# While this was not written for NCL purposes, it probably should have been.
#
# 11/19/2016

if [[ $# < 1 ]]; then
    echo "Dude.  Not enough arguments."
    echo "USAGE: ./ipconv.sh <ip address>"
    echo "EXAMPLE: ./ipconv.sh 192.168.1.1"
    exit
fi

ip_addr=$1
a=$(echo ${ip_addr} | tr "." " " | awk '{ print $1 }')
b=$(echo ${ip_addr} | tr "." " " | awk '{ print $2 }')
c=$(echo ${ip_addr} | tr "." " " | awk '{ print $3 }')
d=$(echo ${ip_addr} | tr "." " " | awk '{ print $4 }')

ab=$(printf "%08d" $(echo "obase=2; $a" | bc))
ah=$(printf "%02x" 0x$(echo "obase=16; $a" | bc))
bb=$(printf "%08d" $(echo "obase=2; $b" | bc))
bh=$(printf "%02x" 0x$(echo "obase=16; $b" | bc))
cb=$(printf "%08d" $(echo "obase=2; $c" | bc))
ch=$(printf "%02x" 0x$(echo "obase=16; $c" | bc))
db=$(printf "%08d" $(echo "obase=2; $d" | bc))
dh=$(printf "%02x" 0x$(echo "obase=16; $d" | bc))

echo "==========================================================="
echo -e "DEC:\t$a $b $c $d"
echo -e "HEX:\t$ah $bh $ch $dh"
echo -e "BIN:\t$ab $bb $cb $db"
echo "==========================================================="

