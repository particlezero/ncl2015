#!/usr/bin/python
#
# Use to decode UTF-8, UTF-16, base64, etc encoded strings.
# (works with multitude of types)
# Also see: url_decode.py
#
# Usage: python ./strenc_decode.py [STRING] [ENCODING]
# Example: python ./strenc_decode.py bXkgY29tcHV0ZXIgaXMgYSBqZXJrZmFjZQ== base64
#

import sys
import unicodedata

def usage():
    print('='*60)
    print("USAGE: python ./strenc_decode.py [STRING] [ENCODING]")
    print("EXAMPLE:  python ./strenc_decode.py bXkgY29tcHV0ZXIgaXMgYSBqZXJrZmFjZQ== base64")
    print('='*60)


if len(sys.argv) < 3:
    print("Dude.  Not enough arguments.")
    usage()
    sys.exit(0)
else:
    data = sys.argv[1]
    enc = "\"" + str(sys.argv[2]) +"\""
    print('='*60)
    print data
    ddecoded = data.decode( enc )
    print ddecoded
    print('='*60)

