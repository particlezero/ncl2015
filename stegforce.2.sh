#!/bin/bash
#
# Written December, 2015 for NCL steganography challenges.  Attempts to
# brute force the file using password list given as second argument.
# Outputs to $HOME/ouput/stegcracked.  This is an attempt to update the
# previous version by allowing user to choose password list at command line
# (as there are multiple steg challenges using different password lists)



stegfile=$1
plist=$2
y=False
while y=False; do
    while read x; do
        echo "Trying ${x}"
        steghide --extract -sf $stegfile -xf $HOME/output/stegcracked -p $x
        RESULTS=$?
        if [[ $RESULTS == *"could not extract any data"* ]]; then
            y=False
        else
            y=True
        fi
    done <$plist
done
