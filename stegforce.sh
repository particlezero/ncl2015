#!/bin/bash
#
# Written December, 2015 for NCL steganography challenge.  Attempts to 
# brute force the file using the 'rockyou.txt' password list as stated in
# the challenge.  Looks for rockyou.txt in home dir and outputs to 
# $HOME/ouput/stegcracked



stegfile=$1
y=False
while y=False; do
    while read x; do
        echo "Trying ${x}"
        steghide --extract -sf $stegfile -xf $HOME/output/stegcracked -p $x
        RESULTS=$?
        if [[ $RESULTS == *"could not extract any data"* ]]; then
            y=False
        else
            y=True
        fi
    done <$HOME/rockyou.txt
done
