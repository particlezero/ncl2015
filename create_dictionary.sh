#!/bin/bash
#
# This script should be modifed/used to add iterative entries
# to existing small dictionaries for password cracking.  Written
# December, 2015 before author understood password mangling options
# typically found in password cracking utilities.
#
# ./create_dictionary.sh <path/to/original/file> <new file>


cat $1 > $2
cat $1 | while read LINE; do
    a=$LINE

# Add one of each "0 - 9" to the end of each word.
#    for i in {0..9}; do
#        echo "${a}${i}" >> $2
#    done
# Add one of each "a - z" to the end of each word.
#    for d in {a..z}; do
#        echo "${a}${d}" >> $2
#    done
# Add every two letter combination of "a - z" to the end of each word.
#    for e in {a..z}{a..z}; do
#        echo "${a}${e}" >> $2
#    done
# Add one of each "A - Z" to the end of each word.
#    for j in {A..Z}; do
#        echo "${a}${j}" >> $2
#    done
# Add every two letter combination of "A - Z" to the end of each word.
#    for h in {A..Z}{A..Z}; do
#        echo "${a}${h}" >> $2
#    done
# Add every two number combination of "0 - 9" to the end of each word.
    for k in {0..9}{0..9}; do
        echo "${a}${k}" >> $2
    done
# Add every three number combination of "0 - 9" to the end of each word.
#    for l in {0..9}{0..9}{0..9}; do
#        echo "${a}${l}" >> $2
#    done
done
