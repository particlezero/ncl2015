#!/bin/bash

# NCL challenge: list of 10 hashes with a password clue: 

# The pattern of the password is "NCL-PC" followed by 2 upper case 
# characters, then a dash, and then 4 digits.

# This script creates the password list.

strng="NCL-PC"
for i in {A..Z}{A..Z}; do
    for x in {0..9}{0..9}{0..9}{0..9}; do
        echo "${strng}${i}-${x}" >> patternwordlist
    done
done

