#!/usr/bin/python
#
# Use to decode those utf-8 urls to find out what characters are there
#


import urllib
import sys
import codecs

def usage():
    print "==========================================================="
    print "USAGE: python ./url_decode.py [URL]"
    print "EXAMPLE: python ./url_decode.py example.com?title=%D0%BF%D1%80%D0%B0%D0%B2%D0%BE%D0%B2%D0%B0%D1%8F+%D0%B7%D0%B0%D1%89%D0%B8%D1%82%D0%B0"
    print "==========================================================="

if len(sys.argv) < 2:
    print("Error.  Not enough arguments.")
    usage()
    sys.exit()
else:
    url = sys.argv[1]
    print urllib.unquote(url).decode('utf8')

