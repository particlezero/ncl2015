#!/bin/bash
#
#
# This script takes every dictionary file in "<home>/dict", and for each
# file, it adds every combination of one number at the beginning and one number
# at the end, creating a new wordlist in "/<home>/wordlists".  Written in
# December, 2015 for NCL challenge before author understood password mangling
# options typically found in password cracking utilities.

FILES=$HOME/dict/*
for file in $FILES; do
    filename=$(echo $file | rev | cut -d "/" -f 1 | rev)
    touch $HOME/wordlists/$filename.wordlist
    while read line; do
        for x in {0..9}; do
            for y in {0..9}; do
                echo $x$line$y >> $HOME/wordlists/$filename.wordlist
            done
        done
    done < $file
done
