# README #

This is a collection of simple bash and python scripts written (thanks in large 
part to help found on the internet) to assist in solving challenges for the
National Cyber League competition, fall 2015.

Most of this code was slapped together and or altered from other examples
found during research.  While I am certain there are other, perhaps more
mature methods for accomplishing these various tasks, this simply represents
my ability to learn these kinds of things on the fly.
